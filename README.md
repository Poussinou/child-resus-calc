This App provides fast calculation of endotracheal tube size, defibrillation energy and drug doses for Paediatric Resuscitation and Rapid Sequence Intubation (RSI) in a pre-hospital paediatric emergency situation.

THIS APP IS INTENDED TO BE USED BY SELF-EMPLOYED DOCTORS/PHYSICIANS. THIS APP IS AN INFORMATION TOOL ONLY AND NOT A SUBSTITUTE FOR PROFESSIONAL JUDGEMENT.

Always enter the child’s weight if possible. Guessing the weight by New APLS Formula is just an approximation. The actual weight may differ markedly.
I did not (yet) implement such a thing as a maximum weight nor a maximum dose. E.g. if you enter a weight of 500 kgs the App will calculate 2000 Joules defibrillation energy, 5 mg Adrenaline, and so on.

When you download and use this App, you agree to these Terms Of Use:

1 Credit
1.1 This document was created using a template from SEQ Legal (http://www.seqlegal.com).

2 No advice
2.1 This App contains general medical information.
2.2 The medical information is not advice and should not be treated as such.

3. No warranties
3.1 The medical information in this App is provided without any representations or warranties, express or implied.
3.2 Without limiting the scope of Section 3.1, I do not warrant or represent that the medical information in this App: (a) will be constantly available, or available at all; or (b) is true, accurate, complete, current or non-misleading.

4. Medical assistance
4.1 You must not rely on the information in this App as an alternative to your own professional judgement as a doctor.

5. Interactive features
5.1 This App includes interactive features that allow users to enter age and weight.
5.2 You acknowledge that, because of the limited nature of communication, any assistance you may receive using any such features is likely to be incomplete and may even be misleading.
5.3 Any assistance you may receive using any of this App’s interactive features does not constitute specific advice and accordingly should not be relied upon without further independent confirmation.

6. Limits upon exclusions of liability
6.1 Nothing in this disclaimer will: (a) limit or exclude any liability for death or personal injury resulting from negligence; (b) limit or exclude any liability for fraud or fraudulent misrepresentation; (c) limit any liabilities in any way that is not permitted under applicable law; or (d) exclude any liabilities that may not be excluded under applicable law.